ZLIMS auto-test project
=======================================

### Setup & Run
- Run bellow script from AUTO-ZLIMS folder to do automatic test.

```
python all_tests_run.py
```

###Project Structure

```
auto-zlims/                             # project root folder
│  all_tests_run.py
│  README.md     
├─config
│  config.ini
├─data   
├─logs
├─pages           
├─poframework
├─report
├─screenshots
├─testcase
│  ├─DNBReport
│  ├─InstrumentDashboard 
│  ├─MainDashboard     
│  ├─Management
│  │  ├─Lookup
│  │  ├─ProjectManagement
│  │  ├─RemoteControl
│  │  ├─ResourceInstance       
│  │  ├─ResourceType     
│  ├─Notification     
│  ├─QCRun         
│  ├─SampleBatch        
│  ├─SampleDashboard      
│  ├─SampleReport        
│  ├─Task       
│  ├─Workflow     
│  └─Zlims_Login
│              
├─tools
│      chromedriver.exe
│      
└─utils
        configfile.py
        HTMLTestRunner.py
        location.py
        PublicMethod.py
```