from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import sys,time
import unittest
sys.path.append('E:\sourcetree\AUTO-ZLIMS\poframework')
from poframework.browser_engine import BrowserEngine
from pages.login_page import LoginPage


class CreateQCRunSuccess(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        browse = BrowserEngine(None)
        self.driver = browse.open_browser()
        pass

    def test_create_QCRun_success(self,username='user',password='123'):
        u'''QCRun 测试'''
        loginpage = LoginPage(self.driver)
        loginpage.type_username(username)
        loginpage.type_password(password)
        loginpage.send_submit_btn()
        time.sleep(3)
        driver = self.driver
        #点击下拉菜单
        driver.find_element_by_xpath("/html/body/md-content/md-sidenav/md-content/ul/li[2]/ul/li/menu-toggle/button/div[1]").click()
        time.sleep(3)
        driver.find_element_by_xpath("//*[@id='docs-menu-Menu_Experiment']/li[4]/menu-link/a/span[2]").click()
        # time.sleep(3)
        driver.find_element_by_name("sampleSource").click()
        #选择Ecoli类型
        driver.find_element_by_xpath(
            "//*[@value='Ecoli']").click()
        #点击"批量选择标签接头"
        driver.find_element_by_xpath(
            "/html/body/md-content/md-content/md-content/div/md-content/form/md-content/md-card[1]/md-menu/button/span").click()
        #选择97-104标签
        driver.find_element_by_xpath(
            "//*[@class='_md md-open-menu-container md-whiteframe-z2 md-active md-clickable']/md-menu-content/md-menu-item[7]").click()
        #点击启动QC Run
        time.sleep(3)
        driver.find_element_by_xpath(
            "/html/body/md-content/md-content/md-content/div/md-content/form/md-content/md-card[1]/div[4]/button/span").click()
        time.sleep(3)


    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True
    @classmethod
    def tearDownClass(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
