from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re,sys
sys.path.append('E:\sourcetree\AUTO-ZLIMS\poframework')
from poframework.browser_engine import BrowserEngine
from pages.login_page import LoginPage
import unittest, time, re


class SampleImportSuccess(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        browse = BrowserEngine(None)
        self.driver = browse.open_browser()
        pass

    def test_sample_import_success(self,username='user',password='123'):
        driver = self.driver
        loginpage = LoginPage(self.driver)
        loginpage.type_username(username)
        loginpage.type_password(password)
        loginpage.send_submit_btn()
        driver.find_element_by_xpath(
            "(.//*[normalize-space(text()) and normalize-space(.)='Toggle collapsed'])[1]/preceding::span[2]").click()
        driver.find_element_by_link_text("样本").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='External-Sample'])[1]/following::span[1]").click()
        driver.find_element_by_xpath(
            u"(.//*[normalize-space(text()) and normalize-space(.)='导入'])[2]/following::label[1]").click()
        driver.find_element_by_id("fileChoose").clear()
        time.sleep(10)
        driver.find_element_by_id("fileChoose").send_keys("E:\sourcetree\AUTO-ZLIMS\data\HMBI-Sample.xlsx")


    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    @classmethod
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
