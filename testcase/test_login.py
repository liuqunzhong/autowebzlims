from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import sys
sys.path.append("..")
sys.path.append("..\..")
from utils import location
from public import login,quit


class ZLIMSLogin(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "http://zlims-isw-01.genomics.cn"
        self.driver.maximize_window()
        self.verificationErrors = []
        self.accept_next_alert = True


    def test_zlims_login(self):
        driver = self.driver
        driver.get("http://zlims-isw-01.genomics.cn/login")
        login.login(self)

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        # self.driver.quit()
        quit.quit(self)
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
