from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re,sys
sys.path.append('E:\sourcetree\AUTO-ZLIMS\poframework')
from poframework.browser_engine import BrowserEngine
from pages.login_page import LoginPage


class ZlimsLoginFailed(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        browse = BrowserEngine(None)
        self.driver = browse.open_browser()
        pass

    def test_zlims_login_failed(self,username='user1',password='1234'):
        loginpage = LoginPage(self.driver)
        loginpage.type_username(username)
        loginpage.type_password(password)
        loginpage.send_submit_btn()
        self.assertEqual(u"用户名或者密码错误!", self.driver.find_element_by_xpath("//*[@id="loginForm"]/div[2]/p/font").)
        print("错误用户名密码登录失败，测试通过！")



    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    @classmethod
    def tearDownClass(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
