import sys,time
import unittest
sys.path.append('E:\sourcetree\AUTO-ZLIMS\poframework')
from poframework.browser_engine import BrowserEngine
from pages.login_page import LoginPage


class LoginSuccess(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        browse = BrowserEngine(None)
        self.driver = browse.open_browser()
        pass

    def test_zlims_login_success(self,username='user',password='123'):
        loginpage = LoginPage(self.driver)
        loginpage.type_username(username)
        loginpage.type_password(password)
        loginpage.send_submit_btn()


    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True
    @classmethod
    def tearDownClass(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
