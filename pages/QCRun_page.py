from poframework.base_page import BasePage
from selenium.webdriver.common.by import By

class QCRunPage(BasePage):
    username_loc = (By.CSS_SELECTOR,"#input_2")
    password_loc = (By.CSS_SELECTOR,"#input_3")
    submit_loc = (By.CSS_SELECTOR,"#loginForm > button")

    def type_username(self, username):
        self.find_element(*self.username_loc).send_keys(username)

    def type_password(self, password):
        self.find_element(*self.password_loc).send_keys(password)

    def send_submit_btn(self):
        self.find_element(*self.submit_loc).click()
