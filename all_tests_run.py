import unittest
import HTMLTestRunner
import os,time,sys

__author__ = "liuqunzhong"

def get_test_cases(dirpath):
    test_cases = unittest.TestSuite()
    # suites = unittest.defaultTestLoader.discover(dirpath, 'TC*.py', top_level_dir=dirpath)
    suites = unittest.defaultTestLoader.discover(dirpath, 'TC001_create_*.py', top_level_dir=dirpath)
    for suite in suites:
        test_cases.addTests(suite)
    return test_cases

if __name__ == '__main__':
    cases = get_test_cases('testcase')
    report_path = os.path.dirname(os.path.abspath('.')) + '/AUTO-ZLIMS/Report/'
    now = time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime(time.time()))
    HtmlFile = report_path + now + "_HTMLtemplate.html"
    f = open(HtmlFile, 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream=f, title=u'Auto ZLIMS Test Report', description=u'详细测试结果如下:')
    runner.run(cases)

