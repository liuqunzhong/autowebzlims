import time
import os.path
from poframework.logger import Logger
from selenium.common.exceptions import NoSuchElementException


logger = Logger(logger="BasePage").getlog()


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


    def quit_browser(self):
        self.driver.quit()


    def forward(self):
        self.driver.forward()
        logger.info("Click forward on current page.")


    def back(self):
        self.driver.back()
        logger.info("Click back on current page.")


    def wait(self, seconds):
        self.driver.implicitly_wait(seconds)
        logger.info("wait for %d seconds." % seconds)


    def close(self):
        try:
            self.driver.close()
            logger.info("Closing and quit the browser.")
        except NameError as e:
            logger.error("Failed to quit the browser with %s" % e)


    def get_windows_img(self):

        file_path = os.path.dirname(os.path.abspath('.')) + '/screenshots/'
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
        screen_name = file_path + rq + '.png'
        try:
            self.driver.get_screenshot_as_file(screen_name)
            logger.info("Had take screenshot and save to folder : /screenshots")
        except NameError as e:
            logger.error("Failed to take screenshot! %s" % e)
            self.get_windows_img()


    def find_element(self, *selector):

        try:
            element = self.driver.find_element(*selector)
            logger.info("The element looked up is %s "% str(selector))
            return element
        except NoSuchElementException as e:
            logger.error("NoSuchElementException: %s" % e)
            self.get_windows_img()

    def type(self, *selector, text):

        el = self.find_element(*selector)
        el.clear()
        try:
            el.send_keys(text)
            logger.info("Had type \' %s \' in inputBox" % text)
        except NameError as e:
            logger.error("Failed to type in input box with %s" % e)
            self.get_windows_img()


    def clear(self, *selector):

        el = self.find_element(*selector)
        try:
            el.clear()
            logger.info("Clear text in input box before typing.")
        except NameError as e:
            logger.error("Failed to clear in input box with %s" % e)
            self.get_windows_img()

    def click(self, *selector):

        el = self.find_element(*selector)
        try:
            el.click()
            logger.info("The element \' %s \' was clicked." % el.text)
        except NameError as e:
            logger.error("Failed to click the element with %s" % e)


    def get_page_title(self):
        logger.info("Current page title is %s" % self.driver.title)
        return self.driver.title


    @staticmethod
    def sleep(seconds):
        time.sleep(seconds)
        logger.info("Sleep for %d seconds" % seconds)